
# Rift for Infinite Campus iOS

Rift for Infinite Campus is an iOS client-app that allows for students who use Infinite Campus to better see their grades in a clear and straightforward way.



---
Currently Rift is in the beta stage with expected deployment date of December 1, 2021


---

## Used By

This project is used by:

- Students who use Infinite Campus


Coming soon

- Parents who use Infintie Campus to see their childs grades


## Tech Stack

**Client:** Swift, SwiftUI, RestAPIs



## Support

For support, email support@schoolscope.org
