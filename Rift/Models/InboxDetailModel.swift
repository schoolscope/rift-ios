//
//  InboxDetailModel.swift
//  Rift
//
//  Created by Varun Chitturi on 10/17/21.
//

import Foundation

struct InboxDetailModel {

    var messageBody: String? = nil
    let message: Message
}
