//
//  InboxModel.swift
//  Rift
//
//  Created by Varun Chitturi on 10/17/21.
//

import Foundation

struct InboxModel {
    
    var messages: [Message] = []
    
}
