//
//  CourseCard.swift
//  CourseCard
//
//  Created by Varun Chitturi on 8/26/21.
//

import SwiftUI

struct CourseCard: View {
    
    let course: Course

    var body: some View {
        Group {
            HStack {
                VStack(alignment: .leading) {
                    Text(course.courseName)
                    Text(course.teacherName ?? "")
                        .foregroundColor(DrawingConstants.secondaryForegroundColor)
                        .fontWeight(.semibold)
                        .font(.caption)
                }
                Spacer()
                VStack {
                    CircleBadge(course.gradeDisplay)
                    Text(course.percentageDisplay)
                        .font(.caption)
                        .frame(width: DrawingConstants.percentageDisplayWidth)
                    
                }
                Image(systemName: "chevron.right")
                    .foregroundColor(DrawingConstants.secondaryForegroundColor)
                    .font(.callout.bold())
            }
            .lineLimit(1)
            .foregroundColor(DrawingConstants.foregroundColor)
            .padding()
        }
        .background(
            RoundedRectangle(cornerRadius: DrawingConstants.backgroundCornerRadius)
                .fill(DrawingConstants.backgroundColor)
        )
        .fixedSize(horizontal: false, vertical: true)
        
    }
    
    private struct DrawingConstants {
        static let foregroundColor = Color("Tertiary")
        static let backgroundColor = Color("Secondary")
        static let secondaryForegroundColor = Color("Quartenary")
        static let backgroundCornerRadius: CGFloat = 20
        static let percentageDisplayWidth: CGFloat = 80
    }
    
}

#if DEBUG
struct CourseCard_Previews: PreviewProvider {
    
    static var previews: some View {
        NavigationView {
            ScrollView {
                ForEach(0..<5) { _ in
                    CourseCard(course: PreviewObjects.course)
                        .padding()
                }
            }
        }
        .previewLayout(.sizeThatFits)
        NavigationView {
            CourseCard(course: PreviewObjects.course)
                .padding()
                .previewLayout(.sizeThatFits)
                .preferredColorScheme(.dark)
        }
            
    }
}
#endif
